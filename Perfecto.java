/**
 *
 * Clase: Perfecto <br>
 * Objetivo Encontrar los números perfectos desde 1 hasta n.<br>
 * @author Juan Parra.
 * @version 1.0
 *
 */

package Laboratorio2;

public class Perfecto{

	/**
	 *
	 * Verifica si el número es entero.<br>
	 *
	 * @param x Número ingresado por el usuario.
	 *
	 * @return True si el número es entero. De lo contrario, retorna false.<br>
	 *
	 */

	public static boolean checkInteger(String x){
		int test = 0;
		boolean bool = false;
		try{
			test = Integer.parseInt(x);
			bool = true;
		}catch (NumberFormatException e){
			System.out.println(e + "\nEl número debe ser entero.");
			bool = false;
		}finally{
			System.out.println("Su entrada: " + x + ".");
			return bool;
		}
	}

	/**
	 *
	 * Verifica si el número es positivo.<br>
	 *
	 * @param x Número ingresado por el usuario.
	 *
	 * @return True si el número es positivo. De lo contrario, retorna false.<br>
	 *
	 */

	public static boolean checkPositive(String x){
		return (Integer.parseInt(x) > 0);
	}


	/**
	 *
	 * Verifica si el número es perfecto e imprime el resultado.
	 *
	 * @param x Número ingresado por el usuario.<br>
	 *
	 */

	public static void output(int x){
		int acum = 0;
		for(int i = 2; i <= x; i++){
			if(x % i == 0){
				acum += x / i;	
			}
		}

		if (acum == x){
			System.out.println("El número " + x + " <====================================== es perfecto.");
		}else{
			System.out.println("El número " + x + " no es perfecto.");
		}
	}


	/**
	 *
	 * Método principal.<br>
	 *
	 * @param args Argumentos iniciales del programa.<br>
	 *
	 */

	public static void main(String[] args){

		try{
			if(checkInteger(args[0]) && checkPositive(args[0])){
				for(int i = 1; i <= Integer.parseInt(args[0]); i++){
					output(i);
				}	
			}else{
				System.out.println("Rectifique el parámetro e inténtelo nuevamente.");
			}
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("No ha ingresado un número.");
		}
	}
}
