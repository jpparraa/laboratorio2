package Laboratorio2;
/*========================================================================== 
 *
 * Encabezado de los Mdulos (Bibliotecas) 
 =========================================================================== 
 */ 
import java.io.*;

/*========================================================================== 
 * Encabezado de la Clase 
 *==========================================================================
 */ 

/** 
 * CLASE:Teclado.java 
 * OBJETIVO: Clase que realiza la lectura de las cadenas del teclado
 * 			 y las convierte en el tipo de dato que se requiere.
 * Curso: Java Nivel Basico.
 * @version 1.2 27/11/2005 
 * @author William Mendoza Rodriguez 
 */ 



public class Teclado{

/*========================================================================== 
 * Atributos Principales 
 *========================================================================== 
 */ 

  /** Buffer de entrada */
  static BufferedReader entrada = new BufferedReader (new InputStreamReader (System.in));
  


/*==========================================================================
 * Documentacin de los Mtodos Analizadores y Modificadores 
 *==========================================================================
 */

  /**
   * public static String leerString (String mensaje)
   * Metodo para leer una cadena del teclado
   * Si se produce una excepcion, vuelve y pregunta.
   * @param mensaje Es la Expresion de pregunta.
   * @return ent. Es la cadena leida del teclado
   */

  public static String leerString (String mensaje) {
    String ent;
    do {
      System.out.print (mensaje) ;
      try {
        ent = entrada.readLine();
        break;
      }
      catch (IOException e) {
        System.out.println (e) ;
      }
    } while (true);
    return ent;
  }

 /**
   * public static int leerInt (String mensaje)
   * Metodo para leer un int del teclado
   * Si se produce una excepcion, vuelve y pregunta.
   * @param mensaje Es la Expresion de pregunta.
   * @return ent. Es la cadena leida del teclado, convertida a int
   */

  public static int leerInt (String mensaje) {
    int ent;
    do {
      System.out.print (mensaje) ;
      try {
        ent = Integer.parseInt (entrada.readLine());//convierto la cadena en int
        break;
      }
      catch (Exception e) {
        System.out.println (e) ;
      }
    } while (true);
    return ent;
  }

 /**
   * public static float leerFloat (String mensaje)
   * Metodo para leer un float del teclado
   * Si se produce una excepcion, vuelve y pregunta.
   * @param mensaje Es la Expresion de pregunta.
   * @return ent. Es la cadena leida del teclado, convertida en float
   */

  public static float leerFloat(String mensaje) throws IOException {
    float  ent;
    do {
      System.out.print (mensaje) ;
      try {
        ent = Float.parseFloat (entrada.readLine());//Convierto la cadena en float
        break;
      }
      catch (NumberFormatException e) {
        System.out.println ("No es un Float") ;
      }
    } while (true);
    return ent;
  }
  
  /**
   * public static double leerDouble (String mensaje)
   * Metodo para leer un double por entrada estandar
   * Si se produce una excepcion, vuelve y pregunta.
   * @param mensaje Es la Expresion de pregunta.
   * @return ent. Es la cadena leida del teclado, convertida en double
   */

  public static double leerDouble (String mensaje) {
    double  ent;
    do {
      System.out.print (mensaje) ;
      try {
        ent = Double.parseDouble (entrada.readLine());//Convierto la cadena a double
        break;
      }
      catch (Exception e) {
        System.out.println (e) ;
      }
    } while (true);
    return ent;
  }
  
} // fin de la clase
