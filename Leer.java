package Laboratorio2;

/**
 * Programa que realiza la lectura del stdinput y lo transforma en varios elementos
 *
 */

public class Leer{

	static String texto = "";
	static String nombre = "";
	static int edad = 0;
	static float promedio = 0.0F;
	static String carrera = "";
	static int semestre = 0;
	static float peso = 0.0F;

	/**
	 *
	 * Utiliza la clase Teclado.java para leer por el teclado.
	 *
	 * @param args Argumentos de línea de comandos.
	 *
	 */

	public static void main(String[] args) throws Exception{

		System.out.println (		"+--------------------------------------+");
		System.out.println (		"|Lee un texto del standar input	|");
		System.out.println (		"|Para poder procesarlo en programa	|");
		System.out.println (		"|Por: Iván Fuentes Quiroz		|");
		System.out.println (		"|Por: William Mendoza Rodriguez	|");
		System.out.println (		"|Modificado por: Juan Parra		|");
		System.out.println (		"+--------------------------------------+");

		do{
			//Pedir datos
			nombre = Teclado.leerString("Ingrese su nombre: ");
			edad = Teclado.leerInt("La Edad: ");
			promedio = Teclado.leerFloat("Promedio: ");
			carrera = Teclado.leerString("Carrera: ");
			semestre = Teclado.leerInt("Semestre: ");
			peso = Teclado.leerFloat("Peso: ");

			//Imprimir datos
			System.out.println("Su nombre es: " +nombre+ ".\n");
			System.out.println("Tiene " +edad+ " años.\n");
			System.out.println("Su promedio es de: " +promedio+ ".\n");
			System.out.println("Su carrera es: " +carrera+ ".\n");
			System.out.println("Smestre: " +semestre+ ".\n");
			System.out.println("Peso: " +peso+ ".\n");
		
			//Solicitar continuación
			System.out.print ("\nDeseas Continuar: [s/n]? ");
			texto = Teclado.leerString("");
		}while(texto.equals("s"));

		System.out.println("Adiós");
	}
}
