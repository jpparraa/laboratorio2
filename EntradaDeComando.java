/**
 *
 * EntradaDeComando
 * Programa que recibe un parámetro desde la línea de comando al ejecutarlo.<br>
 * Uso: java EntradaDeComando <n>.
 * donde <n> debe ser un número entero.<br>
 *
 * @version 1.2
 * @author Iván Fuentes Quiroz
 * @Acondiciono William Mendoza Rodriguez
 *
 */

package Laboratorio2;

	/**
	 *
	 * Programa que espera recibir un parámetro de tipo entero.<br>
	 * En caso de no recibirlo informa el uso del programa o indica que el valor no se pudo codificar.
	 *
	 */

public class EntradaDeComando{


	/**
	 *
	 * Método principal. Recibe un parámetro numérico y lo imprime por consola.
	 *
	 * @param args Argumentos de línea de comandos.
	 *
	 */

	public static void main(String args[]){

		int valor;		//Valor númerico del parámetro de entrada.
		String caracter;	//Carácter recibido como parámetro.

		if(args.length == 0){	//En caso de que NO reciba parámetro.

			System.out.println("Uso: java Laboratorio2.EntradadeComando <n>");

		}else{			//En caso que reciba parámetro.

			caracter = args[0];

			try{

				caracter = caracter.trim();		//Quitar espacios en blanco.
				valor = Integer.parseInt(caracter);

			}catch(Exception e){

				System.out.println(e.toString() + "\nUso: java EntradaDeComando <n> \n Valor incorrecto: " + caracter);
				System.exit(1);

			}

			System.out.println("Valor correcto: " + caracter);
		}
	}
}
